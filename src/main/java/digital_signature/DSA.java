package digital_signature;

import java.security.*;
import java.security.interfaces.DSAPublicKey;

public class DSA {

    public static KeyPair buildKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("DSA");
        keyGenerator.initialize(1024);
        return keyGenerator.genKeyPair();
    }

    public static byte[] sign(PrivateKey privateKey, byte[] message) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signAlgorithm = Signature.getInstance("DSA");

        signAlgorithm.initSign(privateKey);
        signAlgorithm.update(message);

        return signAlgorithm.sign();
    }

    public static boolean verify(PublicKey publicKey, byte[] message, byte[] signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature verifyAlgorithm = Signature.getInstance("DSA");

        verifyAlgorithm.initVerify(publicKey);
        verifyAlgorithm.update(message);

        return verifyAlgorithm.verify(signature);
    }
}
