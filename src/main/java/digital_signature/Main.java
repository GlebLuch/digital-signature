package digital_signature;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {

        KeyPair keyPair = DSA.buildKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        Scanner scanner = new Scanner(System.in);
        String message = scanner.nextLine();

        byte[] signature = DSA.sign(privateKey, message.getBytes(StandardCharsets.UTF_8));

        if(DSA.verify(publicKey, message.getBytes(StandardCharsets.UTF_8), signature)) {
            System.out.println("Подпись верна сообщению");
        }
        else System.out.println("Подпись не верна сообщению");
    }
}
